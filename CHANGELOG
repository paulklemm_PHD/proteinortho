2010
	12-17: Proteinortho V4.18 - Source code
2011
	01-12: Proteinortho V4.20 - Source code
		Support for NCBI blast+
		minor bugfixes
	08-16: Proteinortho V4.22 - Source code
		Added option to output the edge list for reciprocal blast alignments
		Added script to output the remaining edge list after clustering
		Added test+ option for make to run a test using blastp+ rather than blast+
		Relaxed criteria for compilation test to deal with different versions of blast
2012
	05-01: Proteinortho V4.25 - Source code
		Compatibility with newer blast+ v2.2.25
		Compatibility with newer versions of gcc
		Reduced default I/O-threads limit to 3
		Some details for better looking output
	06-05: Proteinortho V4.26 - Source code
		Added -singles option, it allows to report single and paralogous genes found in one species only

2013
	12-17: Proteinortho V5.0 - Source code
		PoFF extension added, which allows to incorporate conserved synteny data (-synteny, requires .gff files for gene positions)
		Default E-value changed from 1e-10 to 1e-05
		Partially reimplemented, more clear variable names and three step model (check/prepare, blast, cluster)
		Changed parameter names (run without options to see manual)
		Pairs will always be reported
		Tree-like structures in the orthology graph are not pruned anymore
2014
	01-27: Proteinortho V5.02 - Source code
		Added -selfblast option to improve prediction of paralogs
	01-31: Proteinortho V5.03 - Source code (BETA)
		Added -singles option to return singleton genes (orphans without any matches)
		Improved multithreading: If more CPUs are present than required for blast jobs, blast's internal subthreads will be invoked
		Improved output: When already present blast output was found, a note is raised to give feedback to the user
	02-12: Proteinortho V5.04 - Source code
		Fixed bugs in the selfblast implementation: Selfblast results obtained using V5.02 or V5.03 (BETA) should be reverified with this version!
		-singles option will add data on singleton genes directly into the results matrix rather than to a separate file
		Added tool to compare graph files (comp_bla.pl)
	03-05: Proteinortho V5.05 - Source code
		Fixed stalling issues for system calls; these could have prevented Proteinortho from finishing an analysis at all
		Added -blastParameters option to define specific blast parameters other than E-Value
		Added -clean switch, it removes temporary files automatically
		Fixed some typos
		Eased thread locking and terminating system
		Added presort of blast results to speed up filtering
		Proteinortho now also parses options when set via --
	04-01: Proteinortho V5.06 - Source code
		Made graph output optional: now it needs to be requested using the -graph switch
		Added a new output file: XXX.descriptions containing ID DESC from FASTA files
		Fixed some typos and description flaws
		Tweaked Makefile
		Special thanks for this update goes to Torsten Seemann, Victorian Bioinformatics Consortium at Monash University, Clayton
	07-01: Proteinortho V5.07 - Source code
		Added a more detailed manual
		Added example data for test
		Minor bugfixes in output data
	07-26: Proteinortho V5.10 - Source code
		speeded up graph processing (a lot)
		improved make test and example files
		fixed minor bugs in tool and manual
		added some bugtracking output data to ease use
	09-23: Proteinortho V5.11 - Source code
		fixed bug when using -singles options with files subfolders
2016
	03-17: Proteinortho V5.12b - Source code
		fixed Makefile (version b)
		fixed code issue in tree builder that prevented it from compiling (version b)
		fixed issue where clustering could take very long or even get stuck
		improved clustering accuracy for small graphs
		added feature to use user-defined temporary paths (-temp=[PATH])
		adapted and re-added UPGMA-tree builder tool for protein presence/absence from the version 4 branch (po2tree)
	04-26: Proteinortho V5.13 - Source code
		fixed issue in graph clustering that sometimes led to random artefacts 
		thanks to David Kraus (MPI Marburg) and Andrey Rozenberg (University of Bochum)
		added hardening modifications for Makefile and added tree builder as install target 
		thanks to Andreas Tille
		Known issues: edges the cleaned graph file (proteinortho-graph) are not reliable at the moment (do not reflect in-program graph)
	08-26: Proteinortho V5.15 - Source code with precompiled binaries (Linux/x64) / Proteinortho V5.15 - Source code only
		output table is ordered by species names and gene names which largely increases readability and comparability
		increased arithmetic accuracy of graph clustering
		added warning before existing outputs are overwritten
		added support for tblastx+ and tblastx legacy 
		thanks to Clemens Thölken
2018    ** Proteinortho6 **
	20. June-4.July
		openmp support (max_of_diag,get_new_x,makeOrthogonal,normalize,getY)
		bitscore integration in the convergence (weighted algebraic connectivity)
		protein output is now sorted in descending degree-order (sort with comparator_pairDoubleUInt)
		getConnectivity: special case checks now if the induced subgraph is complete (K_n)
		added various test functions
	5. July
		added kmere heuristic for splitting groups in proteinortho_clustering. After the calculation of an fiedler vector, the kmere heuristic splits the graph not only in the positive and negative entries of the vector but in k clusters. k=2 -> the original split (without the purity).  
	16. July
		added LAPACK support for CC with less than 2^15 nodes (since it uses quadratic space -> (2^15)^2=2^30) for the calculation of the algebraic connectivity.
		added all other proteinortho files to this repository.
		graphMinusRemoveGraph.cpp implements proteinortho5_clean_edges2.pl in c++
	23.July
		openMP support for laplacian declaration (for lapack).
		'make test' clean up.
		jackhmmer, phmmer, diamond, usearch support.
	24 July
		last integration.
		phmmer+jackhmmer fix/workaround (there is no local identity in the output -> disabled).
		proteinortho.pl : set cluster algorithm to weighted-mode as default.
	30. July
		rapsearch integration.
		proteinortho_clustering.cpp : -ramLapack is now -ram and is the threshold for laplace matrix + graph struct. 
		added dynamic memory management (proteinortho.pl + clustering.cpp) using the free -m command (if exists) 
	31. July
		rapsearch fix (wrong order of db and q)
		purity is back, now 0.1 (and fallback function, if all nodes are below purity threshold -> remove purity for this connected component)
		more options for proteinortho.pl -p=diamond-moresensitive|usearch-ublast
	9. Aug
		topaz integeration.
		all DBs now have the blastmode in name (colliding names) as well as the tmp files generated by the blastalgos.
	10. Aug
		Orthology XML integration. Added the option -noxml for not generating the orthology XML format.
	13. Aug
		bugfix usearch/ublast: removed the description from the gene name (formatU.pl). bugfix rapsearch: forced to create an output .m8 file if there are no hits found.
		allowedAlphabet check in read_details in check_files. E.g. diamond expects aminoacid characters -> found a gene with only nucleotide characters -> WARNING. E.g. blastn+ expects nucleotide characters -> found non nucleotide characters -> ERROR.
	22. Aug
		removed phmmer and jackhmmer. 
		removed the -p=diamondmoresensitive option, since it is equivalent to -p=diamond -subpara='--moresensitive'.
#       redesigned the multithreading system: 
#       -cpus=x -> spawn round(sqrt(x)) workerthreads with each ceil(sqrt(x)) (different for the last workerthread ...) cores for blast. 
#       removed threads_per_process function.
	8. Sep
		proteinortho_clustering: introduced multithreading in partition_graph() -> generate k CC and compute the lapack dsyevx in parallel (1. Memory check 2. if a large CC is found -> 1 power iteration with all cores 3. else do k lapack.). New const variable lapack_power_threshold_n for determining large CC for the power iteration.   
	4. Okt
		improvement in the memory calculations. 
		BUGfix in the DFS calculation (recursion in c++ failed with segmentation fault if the recursion was too deep) -> now iteratively (memory ineffciently) with Queue
	10. Okt
		DFS -> BFS since recursive calls can only be so deep.
	18. Okt
		purity is now 1e-7, evalue 1e-8 (http://people.sc.fsu.edu/~jburkardt/c_src/power_method/power_method_prb.c)
		kmere heuristic minNodes = 2^20 (~1e+6), kmere now checks if the "normal" split would result in a good partition.
	30. Okt
		- removed the memory manager for proteinortho_clustering, instead a simple n threshold manages the power/lapack algorithms
		now all CC are calculated for power/lapack (no frequent restart), dynamic for loop for lapack
	7. Nov
		dsyevr instead of dsyevx (rrr algorithm now)
		remove graph bugfix (each thread is now assigned an own ofstream (shared_ptr needed -> c++11 needed))
	13. Nov
		OMP_PROC_BIND=close for multi-socket systems (change the cpu affinity of openmp to close -> each new thread spawn next to the last one, instead of randomly)
	15. Nov
		Blat support (step=2), the evalues cannot be preset as a parameter but appear if -out=blast8 is set.
	16. Nov
		proteinoprtho 6.0 alpha release
	28. Nov
		Makefile update (lapack zipped,...)
	5. Dec
		MCL integration (-mcl option in proteinortho.pl)
		XML bugfix (species with . in the name did confuse the xml parser)
	6. Dec
		MCL pre/postprocessing (src/do_mcl.pl)
		double -> float in proteinortho_clustering.cpp
		weights are unsigned shorts again (only the last commit was unsigned int) proteinortho_clustering.cpp
	10. Dec
		gff4fasta update: Test for additional naming schemes
	20.-21. Dec
		src/do_mcl.pl performance increase, dev/blastgraph2CCblastgraphs.cpp improvement
		orthoXML improvement (still not accepted by orthobechmarkproject)
	25. Dec
		no lapack version (src/proteinortho_clustering_nolapack.cpp)
		no cmake version (make all_nocmake, needs lapack installed)
2019
	9. Jan
		pow_n replaced with powLapD (graph density threshold instead of number of nodes)
	11. Jan
		mmseq2 integration (proteinortho.pl) -p=mmseqsp or mmseqsn 
	22. Jan (uid:296)
		Added CHAGEUID for a commit specific id. (update_CHANGEUID.sh can be found in snippets, use 'find . -maxdepth 2 | perl -lne '{if($_=~m/^.*(\.pl|\.cpp|\.c|\.h|\.md|\.txt|Makefile|CHANGELOG)$/){print $_;}}' | entr bash update_CHANGEUID.sh')
		BUGfix: weird sort behaviour dependant on locale (LC_All,LC_NUMERIC). Fix: $ENV{'LC_All'}='C';
	23. Jan (uid:492) v6.0a
		small fix for get_po_path
	4. Feb (uid:724)
		-ram is back for memory control of proteinortho_clustering (restricts the memory usage of LAPACK and the input graph), works also for proteinortho.pl -ram
	14. Mar (uid: 1034)
		-tmp is now working better (tmp_dir now generates a directory with all temporary files inside)
		read_details now checks if the input files are faa and fna type based on the -p algorithm (diamond only uses faa files etcpp) IF -checkfasta
	20. Mar (uid: 1174)
		static versions (Linux/x64) of all binaries are now included in the repository
		Makefile now compiles first against /usr/lib/liblapack statically then it tries to recompile src/lapack automatically with 'make'
	26. Apr (uid: 2239)
		now supports -minspecies fully (proteinortho.pl argument)
		fix no_lapack_proteinortho_clustering
		po2html integration
	28. Apr (uid:2349)
		Makefile now builds in src/BUILDS/$uname depending on the system (Linux/Darvin). Now I can include precompiled binaries for mac and linus at the same time.
	1. Mai (uid:2488) v6.0b
		proteinortho is now part of the bioconda repository
		grab_proteins.pl makeover for brew integration
	6. Mai (uid:2821) v6.0
		finally proteinortho2xml.pl is working correctly.
		clean up proteinortho_do_mcl.pl
		renamed all files, such that every program starts with proteinortho !
	11. Mai (uid:3023) v6.0.1
		html improvement (now you can display alternative names from the fasta files)
		new minspecies default (1)
	19. Mai (uid:3063)
		proteinorthoHelper.html
	21. Mai (uid:3080)
		minspecies 1 fix (previously 1 => disabled minspecies calculations)
	22. Mai (uid:3140)
		-selfblast generates duplicated hits -> automatically calls cleanupblastgraph
	3. Juni (uid:3142)
		refined error messages on duplicated inputs, <2 inputs
	27. June (uid:3492)
		proteinortho6.pl now writes databases (-step=1) into -tmp directory if system call failed.
		fixed small issue that tmp directories are created inside eachother. 
		better stderr outputs e.g. if blast fails -> try -check ...
	1. July (uid:3511)
		fixed the -ram issue (used free memory, now total memory) in case there is a swap using up all free memory (also proteinortho_clustering now throws a warning not a error)
	10. Juli (uid:3697)
		fixed proteinortho_grab_proteins.pl: -tofiles option now escapes if -exact, replaced chomp with s/[\r\n]+$//
		proteinortho_grab_proteins.pl speedup for -exact and a given proteinortho file
		proteinortho6.pl replaced chomp with s/[\r\n]+$//
		proteinortho_clustering.cpp fix bug that only uses lapack if -pld is set, regardless of the value.
	11. Sept (uid: 3813)
		updated shebang of ffadj such that python2.7 is used directly (ffadj fails if called with higher version of python)
		-p=blastp is now alias of blastp+ and legacy blast is now -p=blastp_legacy (blastn is equivalent)
		Makefile: static now includes -lquadmath
	25. Sept (uid: 3899)
		synteny update to python3 (but the code looks fishy, the -synteny option now gets a deprecated warning)
		proteinortho now only print html for <10 files automatically and otherwise only gives the option
	4. Nov (uid: 4020)
		FIXED: sometimes the python3 version produces one edditional edge (global defintion of ALPHA). Special thanks for this update goes to Daniel Doerr for fixing this.
	25. Nov (uid: 4030)
		added proteinortho_history
		the synteny option ffadj is now not depricated anymore
	10. Dec (uid: 4196)
		improved proteinortho_history
		removed the new diamond spam
		+ added proteinortho_summary.pl for a summary of proteinortho-graph on species level.
2020
	24. Jan (uid: 4699)
		added the -isoform option 
		added -p=autoblast
	6. Feb (uid: 4745)
		autoblast and -cov now are working together (nucl vs prot -> coverage is in aa length, nucl vs nucl -> cov is in nucl lengths, converted all lengths to aa and alignmentlen/3 for nucl vs nucl)
	30. March (uid: 4748)
		--keep fix : temporary blast files are renamed after calculation as in proteinortho5 : s/.tmp// *tmp
	1. April (4788)
		Fixed system calls for weird input names that could be interpreted by sh (names containing e.g. |)  
		-p=mmseqsp --keep fix : mmseqs converts ids automatically (sp|XXXXX|YYYY -> XXXXX). Updated proteinortho_singletons.pl
	17. April (4798)
		proteinortho now finds corresponding binaries with more edge-cases (weird PATH variable ...)
		--help update
	29. April (4799)
		improved error messages (e.g. for missmatching files with --synteny)
		fixed small bugs for --synteny and the *.summary, *.html files
	12. June (4999)
		reduced the IO work by directly importing the diamond results to proteinortho (no temporary file is generated, except if -keep is set). 
		same ^ for ncbi-blast+ 
		added -mtune -march g++ compiler options for the clustering script
	18. June (5000)
		the -mtune and -march options are now optional due to some incompatibility...
	14. July (5029)
		fixed a small bug, s.t. if write permission is missing in the directory of the fasta files, then the database files are generated in the tmp directory properly.
	4. Aug (5090)
		proteinortho_clustering now properly displays progress (Clusterin: 10%, 20% ...)
		small adjustments to the error outputs
	12. Aug (5100)
		proteinortho2html.pl improvements
		proteinortho.tsv is now sorted by first 3 columns
		proteinortho2xml.pl bug fix
	18. Aug (5102)
		added a warning if -keep is used and -cpus > 8 
		removed an error if the input has whitespaces in the name (should not be a problem anymore)
	31. Aug (5110)
		fixed a bug that the html version prints out ids encapsuled with parenthesis, while the proteinortho_grab_proteins.pl does not understand such queries.
		refined the Makefile for proteinortho_clustering, now it additionally tests if the program is executable with the -test option		
	12. Oct (5122)
		enhancement of the Makefile (more verbose, added standard compiler flags, cleanup)
	2. Dec (5195)
		fixing proteinortho_grab_protein.pl (https://gitlab.com/paulklemm_PHD/proteinortho/-/issues/41)
		and ids that are not found are printed to STDERR 
2021
	3. Jan (5200)
		fixed output sorting (*.proteinortho.tsv)	
	8. Jan (5378) 
		fixed a bug (https://gitlab.com/paulklemm_PHD/proteinortho/-/issues/42), i.e. -synteny option in combination with a -project name with a "." did let ffadj fail.
		fixed a bug: -silent is now silent again
		output files are overwritten instead of appended (which makes no sense)
		selfblast option does now ignore hits between the same protein automatically (before cleanupblastgraph was used)
		proteinortho.summary will be omitted if -nograph is set (there is no graph to generate a summary from)
	10. Jan (5379)
		last update introduced a bug that removes the *.blast-graph if --step=3 is used
	29. Jan (5399)
		fixed a bug (https://gitlab.com/paulklemm_PHD/proteinortho/-/issues/44) involving the --isoform=trinity option (search pattern was too strict), thanks to Sasha Sh !
	3. Feb (5444)
		fixed another --isoform=uniprot bug + more STDERR output 
	15. Feb (5544)
		new -step=4 : for each orthology group (of proteinortho.tsv) build a hmm profile and search in the input files to enrich the group.
		proteinortho_grab_proteins.pl now supports multiple cpu threads (-cpus)
	6. April (5584)
		fixed a bug, where generated databases are always overwritten (https://gitlab.com/paulklemm_PHD/proteinortho/-/issues/48). Thanks to Bjoern 
		fixed another bug that caused the compilation of proteinortho_clustering to fail on CentOS (OMP_PROC_BIND=true) (https://gitlab.com/paulklemm_PHD/proteinortho/-/issues/39). 
	20. June (5594)
		Makefile modification to account for issue 51+52 (https://gitlab.com/paulklemm_PHD/proteinortho/-/issues): the -mtune and -march option are now totally optional, use 'make MTUNEARCH=TRUE all' to enable those features.
	7. July (5630)
		Improving error messages for step=1 and 2.
	23. Dec (5699)
		Fixed a bug for fasta headers with comma (generate a _clean version with ; instead of ,)

2022
	18. Jan (5733)
		Fixed a bug if a faa sequence only contains fna symbols (#55), now diamond will automatically restart with the --ignore-warnings option. 
		Added a new option to add parameters to the database generation step 2 (--subparaMakeBlast), thanks to @kullrich
		Fixed 2 small bugs (a) binaries could not be found in $PATH (changed whereis to which) and (b) the database generation never fails (now it does...)
	19. Apr (5800)
		small improvement to the --snyteny option, thanks to Todd Kitten, Ph.D.
	11. May (5805)
		small bugfix: ignores gff files if given as input files (gff files are automatically detected by name), this fixes a bug in galaxy (using the -syteny option), thanks to Marvin!
	19. May (5815)
		proteinortho detects if input files are not fasta formatted and throws an error message instead of crashing
	9. June (5930) v6.1.0
		major:
		main overhaul of the multithreading system of proteinortho_clustering.cpp now using c++11 worker threads instead of a plain openMP. The new system now uses the provided cores more efficiently
		added -core parameter to proteinortho_clustering.cpp (and the main perl script): stop clustering if a group would split into groups that dont span all species and only output groups that span across all species

		small:
		added --identical to proteinortho6.pl: return only entries that are 100% identical
		added --range= to proteinortho6.pl: 
		small improvements to log output of proteinortho6.pl and quality of life changes, e.g. -sim=90 is automatically corrected to -sim=.9
		added a new -core, -noco and -singles feature to proteinortho_grab_proteins.pl to extract core proteomes more easily
		update to proteinortho_summary.pl: now it displays descriptive statistics about the group file (automatically detected based on the input name), e.g. number of groups that span across all species (core groups)
		proteinortho_clustering.cpp now uses double precision as fallback
		removed doxygen
		-h and --help are now the same (-h previously displayed a short usage)
		
		bugfixes:
		fixed a bug where 'proteinortho_clustering -kmere 0' still used the kmere heuristic as fallback + -exactStep3 is now the default
		fixed a bug where a multithreaded BLAS libraries (if linked against LAPACK) oversubscribes the -cpu thread limitation, thanks Hendrick
	29. June (5990) v6.1.1
		-core now also is applicable to groups that are sub-optimal (that do not span all species initally).
		-small adjustments to summary output
		-added -coreMaxProt -coreMinSpecies to the clustering algorithm for fine-tuning the -core option
		-added a progress output back to the clustering step (and it is enabled by default)
		-small adjustments the multithreading system
		-improved manual/html
		-proteinortho_clustering checks for OMP
		-updated -02 to -03 -funroll-loops
		-removed the OMP_PROC_BIND flag as it sometimes harms more than helps
	12. Sept (5999) v6.1.1
		- new feature -inproject, you can now specify an input project with a different output name (-project)
		- error output improved for -synteny (as requested by #62, thanks to sanyalab)
	19. Sept (6020) v6.1.1
		- proteinortho will throw an error if you all input files only contain one entry (e.g. full genomes are not orthologous).
		- read_details+generate_indices of step 1 is now parallel if there are more jobs than cores
		- proteinortho_summary.pl improved output logs
		- improved README, --help output
		- improved proteinortho_history including now the *.info parameters
	22. Sept (6030) v6.1.2
		- new make test cases for step=3
		- removed -purity from proteinortho6.pl
		- proteinortho_clustering: 
			- removed purity (now purity is determined automatically), 
			- impoved clustering based on purity threshold (groupZero is now added back to the QUEUE)
			- BUGFIX: for the rare case that the +/- components of the fiedlervector are not connected (~1%)
			- BUGFIX: for clustering without purity (previously one cluster was omitted)
	24. Sept (6040) v6.1.2
		- new: automatic prurity detetection (proteinortho_clustering)
		- BUGFIX: remove.graph was missing edges removed from purity
	24. Okt (6041) v6.1.2
		- BUGFIX: proteinortho2html some times got the , wrong in the pop-up window.
	4. Nov (6042) v6.1.3
		- small bugfix: when cleaning data on symlinks, the new "X_clean" files are generated in abs_path which may not be in the input directory! 
		- removed the precompiled proteinortho_treeBuilderCore as virustotal is not happy with this file (false positive hit: #65, thanks to Björn for pointing this out)
		- small bugfix: subparaBlast with diamond and e.g. fast did not work previously
	15. Nov (6045) v6.1.3
		- small adjustments to the upper limit of the automatic purity detection system (0.1 -> 0.12)
		- added the with and without lapack test back to the Makefile (make test or make test_step3)
		- refined feature for proteinortho_clustering -maxnodes: if max-nodes are exceeded, remove the worst 10% of edges of this CC (by weight) until the CC breaks into smaller CC
	23. Nov (6051) v6.1.4
		- small bugfix: newlines of the output of proteinortho_summary.pl are now printed to STDOUT instead of STDERR, thanks to Matthias Bernt !9
		- bugfix: for large project the step=1 could soft lock itself sometimes caused by the log_worker (if used with many cores) #69
	23. Nov (6056) v6.1.5
		- bugfix: proteinortho_clustering -core will split before evaluating the cluster
		- improvement: proteinortho_clustering the new purity detection was too careful (resulting in very long runtimes), now only a fixed number of purities are tested improving the runtime dramatically for large inputs
		- bugfix: the bug of #69 was not fixed entirely
	23. Nov (6060) v6.1.5
		- new feature for proteinortho_clustering: -abc enables a abc type graph input (undirected)
		- some minor adjustments to the -core option of proteinortho_clustering
	26. Nov (6065) v6.1.5
		- fixed a bug that sometimes will result in a suspended but running proteinortho instance (diamond needs separate WD for each call)
	26. Nov (6066) v6.1.6
		- small bugfix for -step=1 and input files with forbidden symbols (invalid fasta files)
	30. Nov (6070) v6.1.6
		- bug #69 did still occure, therefore a new wrapper for diamond, blast, ... is introduced that tests for stalemates and restarts the program a few times until it will fail.
		- ci/cd clean up, introduced a own docker-worker for testing  
	1. Dec (6075) v6.1.6
		- introduced a new default value for maxnodes (species^2) for proteinortho_clustering, usually this improves the runtime by a lot
		- some minor improvements to proteinortho_clustering
		- improved stderr output for proteinortho6.pl
		- added -threads_per_process option to proteinortho6.pl to fine tune the threading system (-step=2 only)
	5. Dec (6080) v6.1.6
		- diamond uses all cores available to generate datasets (step=1), this is now restricted to the specified number of cores (-cpus) 
		- new feature: a fallback system for the blast commands of step=2: if e.g. a diamond calls zombifies (0% cpup running without terminating), proteinortho automatically detects this and restarts this comparison a few times instead of running indefinitely waiting on diamond
	6. Dec (6081) v6.1.6
		- disabled the fallback system for mmseqs as mmseqs spawns the true workload in another thread
	12. Dec (6082) v6.1.6
		- fixed a bug introduced with 6.1.3 where proteinortho_clustering maxnodes option can lead to infinite loops 
		- small improvement to the runtime of proteinortho_clustering
	13. Dec (6085) v6.1.7
		- fixed a bug where -p=mmseqsp results in empty output 
	15. Dec (6086) v6.1.7
		- fixed a bug where multiple -jobs can delete the cache of other job-runs (if used without -keep). Now -jobs defines different individual caches
	22. Dec (6087) v6.1.7
		- improved error output if -step=2 fails (includes now the the $! error message), see #70, thanks to Matthias Bernt
	25. Dec (6088) v6.1.7
		- minor changes to step=2 stderr messages
	28. Dec (6088) v6.1.7
		- added a wait on each kill call of proteinortho_exec
2023
	10. Jan (6091) v6.1.8
		- added some fallback for the abs_path trick (abs_path "$file " = does not follow sym. links)
		- added a check to search for database files right before -step=2 main loop
	18. Jan (6092) v6.1.8
		- added the -tsv option for proteinortho2html.pl to generate a tsv output with the header information of the fasta files
	25. Jan (6093) v6.1.8
		- added a new option to fix the weird HPC "File not found" bug for some HPC systems.
	26. Jan (6094) v6.1.8
		- the weird HPC bug was just an error with the HPC system and not proteinortho...
		- start of the integration of interproscan and dali-blast
	3. Feb (6100) v6.1.8
		- increased the minimal default value of -maxnodes (proteinortho_clustering) for low number of input files (#74), thanks to VLoegler
		- improvements to the parsing (input/output) of the FF-adj algorithm (-synteny) for issue #71
	27. Feb (6103) v6.1.8
		- fixed a bug where proteinortho_clustering was not picking up the correct total scores from the header in combination with -synteny, resulting in "unsigned short overflow" warnings.
		- improved proteinortho_clustering -abc
		- fixed a bug involving the proteinortho_clustering -maxnodes heuristics
	1. March (6104) v6.1.8
		- improved a error message regarding forbidden symbols #77, thanks to Teodor Alling
	3. March (6105) v6.1.8 -> now v6.2.0
		- proteinortho_history.pl sets coverage to 0 if the input fasta files are missing, now it will warn the user if this happens (thanks to VLoegler)
		- improved the -maxnodes heuristic
		- fixed a bug where -maxnodes 2 can lead to an infinite loop.
		- improved the debug output of the clustering algorithm (includes now the CC id and does not overlap if multiple cores are used)
		- fixed a bug where a very small connectivity got accepted although they are below the param_conn value (negative connectivity previously encoded that minspecies was applicable) 
			- you can check if you have groups with that bug using (for a default connectivity of 0.1): LC_ALL=C awk -F'\t' '$3<0.1 && $1!=$2{print $1,$2,$3}' myproject.proteinortho.tsv | wc -l
	4. March (6106) v6.2.0
		- the conn parameter now takes values in the scientific form like 1e-10
	5. March (6107) v6.2.0
		- simplified calculations for conn=0 (minspecies is irrelevant)
	8. March (6108) v6.2.0
		- improved upon the removeLargeEdges heuristic behind the maxnodes parameter (changed from recursion to direct calculation)
		- fixed a bug introduced around 6.1.7 where -p=ublast and -p=usearch always results in a missing database error although the databases are present
	21. March (6109) v6.2.1
		- fixed a bug where the power iteration could only use a single core at most. On the flip side now there is an inflation of threads if power is used (usually around cpus**2) but at most cpus threads are used. This is because a single thread of the dispatch_queue uses openMP pragma with all cores. No oversubscription is observed just an inflated number of S threads.
		- clean up + more documentation for proteinortho_clustering
	22. March (6110) v6.2.2
		- hotfix: very rarely the proteinortho_clustering core dumps at the end (while return EXIT_SUCCESS on specific systems)
		- some new test functions for proteinortho_clustering -test
	24. March (6112) v6.2.3
		- for the removeLowQualityEdges algorithm (-maxnodes) in proteinortho_clustering: added the Smirnov-Grubb test to detect outliers in the set of edges of a large connected component to guide the heuristic intially.
		- bugfix: very large connected components (usually if manually disabling the maxnodes threshold) sometimes did lead to infinte loops in the dispatch_queue
		- for large connected components that observe bad splits, test if pure Smirnov-Grubb results in a better split
		- added new test functions in the Makefile
		- fixed a bug for the -singles involving weird protein names (e.g. containing ',') 
	28. March (6113) v6.3.0
		- fixed a bug where power iteration results in cpu oversubscription for small graphs (per default power is only be used for large graphs)
		- added the lollipop heuristic for large graphs
	14. Apr (6114) v6.3.0
		- fixed a incovenience/bug for testing where -lapack 0 does result in a power iteration with only one core (should then force all cores to be used), corresponding to '-lapack 0 -lapPowD 1' (forcing all cores to be used)
	28. Apr (6115) v6.3.0
		- improved a debug function (-background)
		- fixed a bug where the lollipop algorithm removes edges and those are missing the the remove.graph (only for power iteration)
	2. May (6116) v6.3.0
		- fixed a bug where -lapack 0 results in a core dump for connected components of size 2 (those do not need to be split)
	11. May (6117) v6.3.0
		- some clean up in proteinortho_clustering
	13. May (6118) v6.3.0
		- added some shortcuts
		- BUGFIX: power iteration in combination with -weighted 0 was wrong intialized
	15. May (6119) v6.3.0
		- small improvement to the BFS algorithm (skipping singletons)
	17. Mai (6122) v6.3.0
		- new option -oneblast and -bin (under development)
	22. Mai (6124) v6.3.0
		- improved -step=2
		- fixed a bug where cpus are set to 1 for the clustering after step=2, introduced with 6122
		- fixed a bug for -oneblast as well as for -bin
		- added a evalue calculation to the -oneblast option (E=nm/2^b, n=query length, m=db size, b=bitscore) and with that a reciprocal filtering step.
	23. Mai (6125) v6.3.0
		- improved -oneblast
	24. Mai (6126) v6.3.0
		- improvement to the -oneblast again
	26. Mai (6127) v6.3.0
		- massive improvements to -oneblast and -bin (experimental new feature -halfblast)
		- improvements to the default step=2: memory requirements are now about half as much !
		- note: new defaults for 6.3.5 will be: -lapack 2 -weighted 0 for clustering and threads_per_process = sqrt(cpus) if cpus>=8
	29. Mai (6128) v6.3.0
		- fixed a bug where mmseqs fails because of missing gene ids, introduced in 6217
	30. Mai (6129) v6.3.0
		- fixed a bug that crashed the script using -oneblast and -bin and multiple -step calls
		- fixed a bug where lapack was used for very large connected components (using -lapack 2) although the maxnodes heuristic should be invoked (resulted in a core dump)
	2. June (6130) v6.3.0
		- improved IO load for step=2
	4. June (6131) v6.3.0
		- new feature: halfblast (calculate the reciprocal hit instead of using blast)
	6. Juni (6132) v6.3.0
		- improved -core algorithm
	7. Juni (6133) v6.3.0
		- new feature -job is now compatible with -step=3 (clustering), distributing the inital CC (after first maxnodes greedy split)
	8. Juni (6134) v6.3.0
		- fixed a bug for the -abc input
	12. Juni (6135) v6.3.0
		- added support for blast-graph.gz
		- added -prepare to the clustering algorithm to split the blast-graph into multiple smaller ones that can be directly digested by proteinortho (instead of using -job)
	15. Juni (6136) v6.3.0
		- adjusted the default parameters in line with the manuscript
		- fixed a bug where -xml did produce nothing
		- renamed the parameter -pseudo = -halfblast and -omni = -oneblast. pseudo is now default and can be disabled with -normal or -nopseudo
	16. Juni (6137) v6.3.0
		- fixed a bug where -synteny fails introduced somewhere between 6135-6316 (unpublished)
	17. Juni (6138) v6.3.0
		- new development feature of proteinortho_clustering -minnodes
		- ci/cd
	20. Juni (6139) v6.3.0
		- fixed a bug introduced around 6135 where old diamond version failed to compute step=2 because --ignore-warnins is not implemented e.g. at v2.0.5, now proteinortho detects and adapts automatically
	30. Juni (6140) v6.3.0
		- adjusted the threads_per_process default value for large project
		- fixed a bug where the clustering was not using all available threads
	2. Juli (6141) v6.3.0
		- adjusted the threads_per_processor parameter
		- adjusted the maxnodes default, this should not scale qudratically with number of species, instead now it is set to species*5 with default minimum of 5k.
	4. July (6142) v6.3.0
		- made the proteinortho-graph optional for large projects as it takes a lot of time (remove graph larger than 5GB)
			- IDEA: remove this step and output the proteinortho-graph directly with the clustering script, no additional time needed (just some more IO) should increase runtime by ~5-10%
	18. Aug (6143) v6.3.1
		- added a directionality check for the pseudo algorithm that speeds up by around 5-10%
	30. Sept (6145) v6.3.1
		- improved error messages (number of CPU cores...)
	6. Dez (6147) v6.3.1
		- fixed some issues with -synteny related problems (windows newline induced errors), see #84, thanks to Xuping Zhou
		- fixed some STDOUT overload in combination with -synteny
		- fixed an error caused by many very large input fasta files (>20k proteins per file)
	11. Dez (6148) v6.3.1
		- removed the auto thread adjustment for the synteny program (as it cannot benefit from more threads)
	13. Dez (6149) v6.3.2
		- added new citation
	21. Dez (6150) v6.3.2
		- documentation update thanks to codepilot
2024
	27. Mar (6151) v6.3.2
		- added more cases to identify the corresponding gff file, thanks to Liu Chang.
	8. Apr (6152) v6.3.2
		- improved ouput if forbidden symbols are present (e.g. stop symbols), thanks to Salvatore Cosentino (issue #91)
	1. May (6153) v6.3.2
		- reduced the IO usage by removing redundant files introduced somewhere in 6.3.x. This should speed things up especially for HPC systems (using the jobs parameter)
	8. Nov (6154) v6.3.3
		- BUGFIX: previously, proteinortho could overallocate CPU threads in the clustering in some cases (resulted from the thread distribution manager in the blast step), fixed thanks to Michael Dondrup for reporting (#93)
	12. Dec (6155) v6.3.4
		- BUGFIX: fixed a problem where n+1 directories *cache*/thread$i_WD were generated for n threads. Fixed a bug with -p=mmseqsp for -cpus=1.
